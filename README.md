# observation
## create observation namespace:
### create a namespace.yaml file 
 <br>apiVersion: v1</br>
<br>kind: Namespace</br>
<br>metadata:</br>
  <br> &emsp; name: observation</br>

## Run the next flug to create the observation namespace:
~~~
kubectl create -f namespace.yaml 
~~~

## Installing Prometheus and grafana
### run the following commands:

~~~
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm repo add grafana https://grafana.github.io/helm-charts

helm repo update
~~~

### Install Prometheus using the following command:

~~~
helm install prometheus prometheus-community/prometheus

kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext --namespace=observation

minikube service prometheus-server-ext -n observation

~~~

### Install Grafana using the following command:

~~~
helm install grafana grafana/grafana --namespace=observation
~~~

### Get the password to your grafana server:

~~~
 kubectl get secret --namespace observation grafana -o jsonpath="{.data.admin-password}" | base64 --decode
~~~

### Get the Grafana URL to visit by running these commands in the same shell:

~~~
export POD_NAME=$(kubectl get pods --namespace observation -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")

kubectl --namespace observation port-forward $POD_NAME 3000
~~~

### Connect to grafane:
go to the link in your cli:
<br>usename: admin</br>
<br>password: (the output from the previous step)</br>


# bonus

## Change user name:
click on the avatar button in the right side on the screan > click admin > change the username to your gitlab user > click on save 

## Change user avatar:
http://en.gravatar.com/